# Project Title

The idea of project was to make an own sotring algorithm in C++.

## Getting Started

Take a git clone on project and take it to your computer. In root run command all and then you can run command
run. The program starts.
It gives you a example of file content before and after sorting.
### Prerequisites

You need a windows or linux computer, in which you can build the project. G++ compiler is also needed
and cmake on your computer.

## Usage

You can replace the sorting file, if you name it as the original and keep the syntax same.

# About the project

This was made by Ilona Skarp for Noroff and Experis Academy Finland.
